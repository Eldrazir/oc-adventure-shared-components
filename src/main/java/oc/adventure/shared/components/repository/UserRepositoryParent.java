package oc.adventure.shared.components.repository;

import oc.adventure.shared.components.entity.UserParentImpl;
import oc.adventure.shared.components.entity.contract.UserParent;

import java.util.List;

public interface UserRepositoryParent<T> {

    T find(int id);

    T findByExtId(String extId);

    List<T> findAllOrderLastNameAsc(int pageNb, int nbPerPage);

    List<T> findAllOrderLastNameDesc(int pageNb, int nbPerPage);

}
