package oc.adventure.shared.components.repository;

import java.util.List;

public interface AdventureRepositoryParent<T> {

	T find(int id);

	List<T> findAll(int pageNb, int nbPerPage);

	
}
