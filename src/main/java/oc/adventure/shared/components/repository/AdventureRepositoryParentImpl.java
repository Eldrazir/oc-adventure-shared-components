package oc.adventure.shared.components.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;

import oc.adventure.shared.components.entity.AdventureParentImpl;
import oc.adventure.shared.components.entity.contract.AdventureParent;

public abstract class AdventureRepositoryParentImpl<T extends AdventureParent> implements AdventureRepositoryParent<T>{

	@Autowired
    protected EntityManager entityManager;

    public abstract Class<T> getEntityTClass();

    
    @Override
    public T find(int id) {
        return  this.entityManager.find(getEntityTClass(), id);
    }

    @Override
    public List<T> findAll(int pageNb, int nbPerPage) {
        return entityManager.createNamedQuery(AdventureParentImpl.QN.FIND_ALL, getEntityTClass())
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    
}
