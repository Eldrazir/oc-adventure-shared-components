package oc.adventure.shared.components.repository;

import oc.adventure.shared.components.entity.UserParentImpl;
import oc.adventure.shared.components.entity.contract.UserParent;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public abstract class UserRepositoryParentImpl<T extends UserParent> implements UserRepositoryParent<T> {

    @Autowired
    protected EntityManager entityManager;

    public abstract Class<T> getEntityTClass();

    @Override
    public T find(int id) {
        return  this.entityManager.find(getEntityTClass(), id);
    }

    @Override
    public T findByExtId(String extId){
        TypedQuery<T> UserTypedQuery = entityManager.createNamedQuery(UserParentImpl.QNP.FIND_BY_EXT_ID, getEntityTClass())
                .setParameter(DBUtils.EXTERNAL_ID_DB, extId);
        return JPAUtils.getSingleResult(UserTypedQuery);
    }


    @Override
    public List<T> findAllOrderLastNameAsc(int pageNb, int nbPerPage) {
        return entityManager.createNamedQuery(UserParentImpl.QNP.FIND_ALL_ORDER_LAST_NAME_ASC, getEntityTClass())
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public List<T> findAllOrderLastNameDesc(int pageNb, int nbPerPage) {
        return entityManager.createNamedQuery(UserParentImpl.QNP.FIND_ALL_ORDER_LAST_NAME_DESC, getEntityTClass())
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }



}
