package oc.adventure.shared.components.entity.contract;

public interface AdventureParent {
	 Integer getId();

	    String getExternalId();
	    
	    String getNameAdventure();

	
}
