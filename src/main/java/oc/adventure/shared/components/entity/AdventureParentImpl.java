package oc.adventure.shared.components.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


import oc.adventure.shared.components.entity.contract.AdventureParent;
import oc.adventure.utils.DBUtils;

@MappedSuperclass
@NamedQueries({ 
	@NamedQuery(
			name = AdventureParentImpl.QN.FIND_BY_EXT_ID, 
			query = AdventureParentImpl.QN.SELECT_FROM
		+ "WHERE a.externalId = :ext_id"
		),
	
	@NamedQuery(
            name = AdventureParentImpl.QN.FIND_BY_NAME_ADVENTURE,
            query = AdventureParentImpl.QN.SELECT_FROM +
                    "WHERE  a.nameAdventure = :name_adventure"
    ),
	 @NamedQuery(
             name = AdventureParentImpl.QN.FIND_ALL,
             query = "SELECT a FROM AdventureImpl a "
     ),


})

public class AdventureParentImpl implements AdventureParent {

	public static class QN {

		public static final String FIND_BY_EXT_ID = "AdventureImpl.findByExtID";
		public static final String FIND_BY_NAME_ADVENTURE = "AdventureImpl.findByNameAdventure";

		public static final String FIND_ALL = "AdventureImpl.findAll";

		public static final String SELECT_FROM = "SELECT a FROM AdventureImpl a ";

	}

	  @Id
	    @Column(name = "id", unique = true, nullable = false)
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer id;

	    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
	    private String externalId;

	@Column(name = DBUtils.NAME_ADVENTURE_DB)
    private String nameAdventure;

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	@Override
	public String getNameAdventure() {
		return nameAdventure;
	}

	public void setNameAdventure(String nameAdventure) {
		this.nameAdventure = nameAdventure;
	}
	
	
	
}
