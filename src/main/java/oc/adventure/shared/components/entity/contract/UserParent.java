package oc.adventure.shared.components.entity.contract;

public interface UserParent {
    Integer getId();

    String getExternalId();

    String getFirstName();

    String getLastName();
}
