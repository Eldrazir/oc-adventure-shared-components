package oc.adventure.shared.components.entity;

import oc.adventure.shared.components.entity.contract.UserParent;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
@NamedQueries({
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_BY_EXT_ID,
                query = UserParentImpl.QNP.SELECT_FROM +
                        "WHERE u.externalId = :external_id"
        ),
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_ALL_ORDER_LAST_NAME_ASC,
                query = UserParentImpl.QNP.SELECT_FROM + UserParentImpl.QNP.ORDER_LAST_NAME_ASC
        ),
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_ALL_ORDER_LAST_NAME_DESC,
                query = UserParentImpl.QNP.SELECT_FROM + UserParentImpl.QNP.ORDER_LAST_NAME_DESC
        ),
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_ALL_BY_LAST_NAME_LIKE_ORDER_LAST_NAME_ASC,
                query = UserParentImpl.QNP.SELECT_FROM +
                        "WHERE u.lastName = :last_name_like " +
                        UserParentImpl.QNP.ORDER_LAST_NAME_ASC
        ),
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_ALL_BY_FIRST_NAME_LIKE_ORDER_LAST_NAME_ASC,
                query = UserParentImpl.QNP.SELECT_FROM +
                        "WHERE u.firstName LIKE :first_name_like " +
                        UserParentImpl.QNP.ORDER_LAST_NAME_ASC
        ),
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_ALL_BY_FIRST_NAME_LIKE_ORDER_LAST_NAME_DESC,
                query = UserParentImpl.QNP.SELECT_FROM +
                        "WHERE u.firstName LIKE :first_name_like " +
                        UserParentImpl.QNP.ORDER_LAST_NAME_DESC
        ),
        @NamedQuery(
                name = UserParentImpl.QNP.FIND_ALL_BY_LAST_NAME_LIKE_ORDER_LAST_NAME_DESC,
                query = UserParentImpl.QNP.SELECT_FROM +
                        "WHERE u.lastName LIKE :last_name_like " +
                        UserParentImpl.QNP.ORDER_LAST_NAME_DESC
        ),
})
public class UserParentImpl implements UserParent {

    public static class QNP {

        public static final String FIND_BY_EXT_ID = "UserImpl.findByExtID";

        public static final String FIND_ALL_ORDER_LAST_NAME_ASC = "VetImpl.findAllOrderLastNameAsc";
        public static final String FIND_ALL_ORDER_LAST_NAME_DESC = "VetImpl.findAllOrderLastNameDesc";

        public static final String FIND_ALL_BY_FIRST_NAME_LIKE_ORDER_LAST_NAME_ASC = "VetImpl.findAllByFirstNameLikeOrderLastNameAsc";
        public static final String FIND_ALL_BY_LAST_NAME_LIKE_ORDER_LAST_NAME_ASC = "VetImpl.findAllByLastNameLikeOrderLastNameAsc";

        public static final String FIND_ALL_BY_FIRST_NAME_LIKE_ORDER_LAST_NAME_DESC = "VetImpl.findAllByFirstNameLikeOrderLastNameDesc";
        public static final String FIND_ALL_BY_LAST_NAME_LIKE_ORDER_LAST_NAME_DESC = "VetImpl.findAllByLastNameLikeOrderLastNameDesc";

        public static final String SELECT_FROM = "SELECT u FROM UserImpl u ";

        public static final String ORDER_LAST_NAME_DESC = "ORDER BY u.lastName DESC";
        public static final String ORDER_LAST_NAME_ASC = "ORDER BY u.lastName ASC";
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true, length = 8)
    private String externalId;

    @Column(name = DBUtils.FIRST_NAME_DB)
    private String firstName;

    @Column(name = DBUtils.LAST_NAME_DB)
    private String lastName;

    public UserParentImpl() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserParentImpl)) return false;
        UserParentImpl that = (UserParentImpl) o;
        return getExternalId().equals(that.getExternalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalId());
    }
}
