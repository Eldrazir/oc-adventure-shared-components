package oc.adventure.shared.components.dto.address.typeaddress;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;

public class TypeAddressOutDto extends TypeAddressInDto implements Serializable {

    @JMap
    private String externalId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public TypeAddressOutDto() {
    }

}
