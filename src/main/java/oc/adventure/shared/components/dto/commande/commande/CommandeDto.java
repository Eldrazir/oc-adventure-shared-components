package oc.adventure.shared.components.dto.commande.commande;

import com.googlecode.jmapper.annotations.JMap;

public class CommandeDto {

    @JMap
    private double totalCost;

    @JMap
    private String stripeEmail;

    @JMap
    private String stripeToken;

    @JMap
    private String currency;

    @JMap
    private String fullAddress;


    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getStripeEmail() {
        return stripeEmail;
    }

    public void setStripeEmail(String stripeEmail) {
        this.stripeEmail = stripeEmail;
    }

    public String getStripeToken() {
        return stripeToken;
    }

    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }
}
