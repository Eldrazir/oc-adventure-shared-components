package oc.adventure.shared.components.dto.user;

import java.io.Serializable;

public class ExistsDto implements Serializable {

    private boolean exists;

    public ExistsDto() {
    }

    public ExistsDto(boolean exists) {
        this.exists = exists;
    }

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }
}
