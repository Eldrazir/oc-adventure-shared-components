package oc.adventure.shared.components.dto.address.address;

public class AddressUserTypeInDto {

    private String userExternalId;


    private String typeAddressExternalId;

    public AddressUserTypeInDto() {
    }

    public String getUserExternalId() {
        return userExternalId;
    }

    public void setUserExternalId(String userExternalId) {
        this.userExternalId = userExternalId;
    }

    public String getTypeAddressExternalId() {
        return typeAddressExternalId;
    }

    public void setTypeAddressExternalId(String typeAddressExternalId) {
        this.typeAddressExternalId = typeAddressExternalId;
    }

}
