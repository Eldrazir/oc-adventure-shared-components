package oc.adventure.shared.components.dto.address.typeaddress;

import com.googlecode.jmapper.annotations.JMap;

public class TypeAddressInDto {

    @JMap
    private String name;

    public TypeAddressInDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
