package oc.adventure.shared.components.dto.role;


import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;
import java.util.List;

public class RoleOutDto implements Serializable {

    @JMap
    private String name;

    @JMap
    private String externalId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
