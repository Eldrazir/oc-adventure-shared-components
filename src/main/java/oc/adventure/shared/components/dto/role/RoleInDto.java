package oc.adventure.shared.components.dto.role;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;

public class RoleInDto implements Serializable {
    @JMap
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
