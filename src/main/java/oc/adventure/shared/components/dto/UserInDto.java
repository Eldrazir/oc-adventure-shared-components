package oc.adventure.shared.components.dto;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;

public class UserInDto implements Serializable {


    @JMap
    private String firstName;

    @JMap
    private String lastName;

    @JMap
    private String phoneNumber;

    @JMap
    private String mail;

    @JMap
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
