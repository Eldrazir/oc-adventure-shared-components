package oc.adventure.shared.components.dto.commande.commande;

import com.googlecode.jmapper.annotations.JMap;
import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionOutDto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class CommandeOutDto extends CommandeDto{

    @JMap
    private String externalId;

    @JMap
    private LocalDateTime paiementDate;

    List<CommandeSessionOutDto> commandeSessionOutDtoList = new ArrayList<>();

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public List<CommandeSessionOutDto> getCommandeSessionOutDtoList() {
        return commandeSessionOutDtoList;
    }

    public void setCommandeSessionOutDtoList(List<CommandeSessionOutDto> commandeSessionOutDtoList) {
        this.commandeSessionOutDtoList = commandeSessionOutDtoList;
    }

    public LocalDateTime getPaiementDate() {
        return paiementDate;
    }

    public void setPaiementDate(LocalDateTime paiementDate) {
        this.paiementDate = paiementDate;
    }
}
