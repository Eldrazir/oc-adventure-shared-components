package oc.adventure.shared.components.dto.user;

import com.googlecode.jmapper.annotations.JMap;

public class UserRegisterLightDto {

    @JMap
    private String firstName;

    @JMap
    private String lastName;

    @JMap
    private String phoneNumber;

    @JMap
    private String mail;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
