package oc.adventure.shared.components.dto.commande.adventure;


import com.googlecode.jmapper.annotations.JMap;

public class AdventureOutDto {

    @JMap
    private String externalId;

    @JMap
    private String nameAdventure;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getNameAdventure() {
        return nameAdventure;
    }

    public void setNameAdventure(String nameAdventure) {
        this.nameAdventure = nameAdventure;
    }
}
