package oc.adventure.shared.components.dto.address.address;


import com.googlecode.jmapper.annotations.JMap;


public class AddressBaseDto {

    @JMap
    private String address;

    @JMap
    private String city;

    @JMap
    private String region;

    @JMap
    private Integer postalCode;

    @JMap
    private String country;



    public AddressBaseDto() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


}
