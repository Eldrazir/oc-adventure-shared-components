package oc.adventure.shared.components.dto.address.address;

import com.googlecode.jmapper.annotations.JMap;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;

public class AddressOutDto extends AddressBaseDto {

    @JMap
    private String externalId;

    private TypeAddressOutDto typeAddress;


    public AddressOutDto() {
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public TypeAddressOutDto getTypeAddress() {
        return typeAddress;
    }

    public void setTypeAddress(TypeAddressOutDto typeAddress) {
        this.typeAddress = typeAddress;
    }

}
