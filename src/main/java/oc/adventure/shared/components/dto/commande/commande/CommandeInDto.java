package oc.adventure.shared.components.dto.commande.commande;


import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionInDto;


import java.util.List;

public class CommandeInDto extends CommandeDto {

    private List<CommandeSessionInDto> commandeSessionInDtoList;

    private String userExternalId;

    public List<CommandeSessionInDto> getCommandeSessionInDtoList() {
        return commandeSessionInDtoList;
    }

    public void setCommandeSessionInDtoList(List<CommandeSessionInDto> commandeSessionInDtoList) {
        this.commandeSessionInDtoList = commandeSessionInDtoList;
    }

    public String getUserExternalId() {
        return userExternalId;
    }

    public void setUserExternalId(String userExternalId) {
        this.userExternalId = userExternalId;
    }
}
