package oc.adventure.shared.components.dto.commande.commandesession;

import com.googlecode.jmapper.annotations.JMap;

public class CommandeSessionDto {

    @JMap
    private int quantity;

    @JMap
    private float price;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
