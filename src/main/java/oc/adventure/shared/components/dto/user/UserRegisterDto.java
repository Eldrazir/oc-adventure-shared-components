package oc.adventure.shared.components.dto.user;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;

public class UserRegisterDto extends UserRegisterLightDto implements Serializable {


    @JMap
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
