package oc.adventure.shared.components.dto.address.address;

import oc.adventure.shared.components.dto.address.user.UserOutDto;

public class AddressUserOutDto extends AddressOutDto {

    private UserOutDto userOutDto;

    public AddressUserOutDto() {
    }

    public UserOutDto getUserOutDto() {
        return userOutDto;
    }

    public void setUserOutDto(UserOutDto userOutDto) {
        this.userOutDto = userOutDto;
    }

}
