package oc.adventure.shared.components.dto.address.user;

import oc.adventure.shared.components.dto.address.address.AddressOutDto;

import java.util.ArrayList;
import java.util.List;

public class UserOutDto extends UserInDto {

    private List<AddressOutDto> addressList = new ArrayList();

    public UserOutDto() {
    }

    public List<AddressOutDto> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<AddressOutDto> addressList) {
        this.addressList = addressList;
    }
}
