package oc.adventure.shared.components.dto.auth;

import java.io.Serializable;

public class AuthBodyInDto implements Serializable {

    private String mail;
    private String password;

    public AuthBodyInDto() {
    }

    public AuthBodyInDto(String mail, String password) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }
    public void setEmail(String email) {
        this.mail = email;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}