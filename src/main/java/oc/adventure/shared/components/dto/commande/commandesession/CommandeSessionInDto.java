package oc.adventure.shared.components.dto.commande.commandesession;

import com.googlecode.jmapper.annotations.JMap;

public class CommandeSessionInDto extends CommandeSessionDto{

    private String sessionExternalId;

    public String getSessionExternalId() {
        return sessionExternalId;
    }

    public void setSessionExternalId(String sessionExternalId) {
        this.sessionExternalId = sessionExternalId;
    }
}
