package oc.adventure.shared.components.dto.commande.user;

import oc.adventure.shared.components.dto.commande.commande.CommandeOutDto;

import java.util.ArrayList;
import java.util.List;

public class UserOutDto extends UserInDto {

    private List<CommandeOutDto> commandeList = new ArrayList();

    public UserOutDto() {
    }

    public List<CommandeOutDto> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<CommandeOutDto> commandeList) {
        this.commandeList = commandeList;
    }
}
