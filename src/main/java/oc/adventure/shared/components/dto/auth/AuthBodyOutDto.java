package oc.adventure.shared.components.dto.auth;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthBodyOutDto implements Serializable{

    @JMap
    private String firstName;
    @JMap
    private String lastName;
    @JMap
    private String password;
    @JMap
    private String mail;
    @JMap
    private String externalId;

    private List<String> roleList = new ArrayList<String>();

    public AuthBodyOutDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<String> roleList) {
        this.roleList = roleList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
