package oc.adventure.shared.components.dto.commande.commandesession;

import com.googlecode.jmapper.annotations.JMap;

import java.time.LocalDate;


public class CommandeSessionOutDto extends CommandeSessionDto {

    @JMap
    private String externalId;

    private String adventureName;

    private LocalDate startDate;

    private LocalDate endDate;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getAdventureName() {
        return adventureName;
    }

    public void setAdventureName(String adventureName) {
        this.adventureName = adventureName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
