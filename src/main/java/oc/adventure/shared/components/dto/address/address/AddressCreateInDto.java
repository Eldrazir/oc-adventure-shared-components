package oc.adventure.shared.components.dto.address.address;

public class AddressCreateInDto extends AddressBaseDto {

    
    private String userExternalId;


    private String typeAddressExternalId;

    public AddressCreateInDto() {
    }

    public String getUserExternalId() {
        return userExternalId;
    }

    public void setUserExternalId(String userExternalId) {
        this.userExternalId = userExternalId;
    }

    public String getTypeAddressExternalId() {
        return typeAddressExternalId;
    }

    public void setTypeAddressExternalId(String typeAddressExternalId) {
        this.typeAddressExternalId = typeAddressExternalId;
    }

}
