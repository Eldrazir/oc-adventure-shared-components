package oc.adventure.shared.components.dto.address.user;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;

public class UserInDto implements Serializable {

    @JMap
    private String lastName;

    @JMap
    private String firstName;

    @JMap
    private String externalId;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public UserInDto() {
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
