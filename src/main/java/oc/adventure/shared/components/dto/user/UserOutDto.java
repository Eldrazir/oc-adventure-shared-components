package oc.adventure.shared.components.dto.user;

import com.googlecode.jmapper.annotations.JMap;

import java.io.Serializable;

public class UserOutDto implements Serializable {

    @JMap // Pour indiquer qu'il doit automatiquement Mapper cet attribut avec l'attribut de même nom de l'entité User (hibernate)
    private String externalId;

    @JMap
    private String firstName;

    @JMap
    private String lastName;

    @JMap
    private String phoneNumber;

    @JMap
    private String mail;

    @JMap
    private boolean isValid;

    public UserOutDto() {
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public boolean getIsValid() {
        return isValid;
    }



    public void setIsValid(boolean valid) {
        isValid = valid;
    }
}
